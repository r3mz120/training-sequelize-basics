const SiscotDB = require("../models").Siscot;
const DepartamentBD = require("../models").Departament;
const ProiectDB = require("../models").Proiect;
const controller = {
  getSiscotiFromDepartament: async (req, res) => {
    DepartamentBD.findByPk(req.params.id, {
      include: SiscotDB,
    })
      .then((siscoti) => {
        res.status(200).send(siscoti);
      })
      .catch((err) => {
        res.status(500).send(err);
      });
  },
  getSiscotiFromProiecte: async (req, res) => {
    ProiectDB.findAll({
      include: SiscotDB,
      //   attributes: ["nume"],
    })
      .then((siscoti) => {
        res.status(200).send(siscoti);
      })
      .catch((err) => res.status(500).send(err));
  },

  getSiscoti: async (req, res) => {
    SiscotDB.findAll()
      .then((siscoti) => res.status(200).send(siscoti))
      .catch((err) => res.status(500).send(err));
  },
};
module.exports = controller;
